###
# Fontana feed visualizer.
###

@Fontana ?= {}

messageTemplate = '<div id="{id}" class="message media well col-md-6 col-md-offset-3">
    <figure class="pull-left media-object">
        <img src="{user.profile_image_url}" width="64" height="64" alt="" class="avatar img-thumbnail">
    </figure>
    <div class="media-body">
        <div class="media-heading">
            <cite>
                <span class="name">{user.name}</span>
                <small class="text-muted">
                    <span class="screen_name">@{user.screen_name}</span>
                    <time class="time pull-right" data-time="{created_at}">{created_at}</time>
                </small>
            </cite>
        </div>
        <div class="text lead"><q><img class="twimg" style="float:right" src="{image}" alt="twimg">{text}</q></div>
    </div>
</div>'

transitionEffects = ['scroll-up', 'compress', 'fade-in', 'hinge', 'lightspeed',
                     'scroll-down', 'slide', 'tilt-scroll',
                     'vertigo', 'zoom-in']


class Fontana.Visualizer
    # Fontana visualizer, takes a container node and a datasource.
    constructor: (@container, @datasource) ->
        @paused = false
        @fetchMessagesTimer = -1
        @animationTimer = -1

    start: (settings)->
        @fetchMessages(true)
        @container.empty()
        @config(settings)
        @scheduleUpdateAllTimes()

    config: (settings)->
        transitionEffects.forEach (cls) =>
            @container.removeClass(cls)
        if settings && settings.transition && transitionEffects.indexOf(settings.transition) > -1
            @container.addClass(settings.transition)
        else
            @container.addClass(transitionEffects[0])

    pause: ->
        if !@paused
            clearTimeout(@fetchMessagesTimer)
            clearTimeout(@animationTimer)
            @paused = true

    resume: ->
        if @paused
            @fetchMessages()
            @animate()
            @paused = false

    stop: ->
        @pause()
        @container.empty()

    # Messages
    fetchMessages: (initial=false)->
        @datasource.getMessages((data) =>
            @renderMessages(data, initial)
            @scheduleFetchMessages())

    renderMessages: (messages, initial=false)->
        messages.forEach((message)=>
            if !$("##{message.id}").length
                if message.entities
                    #message.text = twttr.txt.autoLink(
                        #message.text, targetBlank: true)
                    if message.entities.media
                      message.image = message.entities.media[0].media_url + ':thumb'
                    else
                      message.image = '/img/one.png'
                else
                    #message.text = twttr.txt.autoLink(message.text, targetBlank: true)
                    message.image = '/img/one.png'
                message.text = message.text.replace /@(\w+)/g, '@<a class="tweet-url username">$1</a>'
                message.text = message.text.replace /#(\w+)/g, '<a class="tweet-url hashtag">#$1</a>'
                #message.text = message.text.replace /http</, '<'
                messageNode = $(nano(messageTemplate, message))
                @updateTime(messageNode)
                @container.append(messageNode))
        if initial
            @scheduleAnimation()

    animate: ->
        messages = $(".message", @container)
        max_tweets = 30


        if messages.length > max_tweets
          x = messages.length - max_tweets
          del = $('.message.prev').slice(0,x)
          del.remove()


        messages.removeClass("next next-one next-two focus prev-one prev-two prev ")
        if !@current
            @current = $(".message:first", @container)
        else
            @current =  if !@current.next().length then $(".message:first", @container) else @current.next()
        next  = @current.next()
        if !next.length
            next = $(".message:first", @container)
        next2 = next.next()
        if !next2.length
            next2 = $(".message:first", @container)
        @current.addClass("focus")
        next.addClass("next-one")
        next2.addClass("next-two")
        next2.nextAll(":not(.focus)").addClass("next")
        prev = @current.prev()
        if !prev.length
            prev = $(".message:last", @container)
        prev2 = prev.prev()
        if !prev2.length
            prev2 = $(".message:last", @container)
        prev.addClass("prev-one").removeClass("next")
        prev2.addClass("prev-two").removeClass("next")
        prev2.prevAll(":not(.next-one):not(.next):not(.focus)").addClass("prev")

        @scheduleAnimation()

    # Time display
    updateAllTimes: ->
        $(".message", @container).each((i, message)=>
            @updateTime(message))
        @scheduleUpdateAllTimes()

    updateTime: (message)->
        time = $(".time", message)
        time.text(Fontana.utils.prettyDate(time.data("time")))

    # Scheduling
    scheduleAnimation: ->
        delay = if @animationTimer == -1 then 0 else 6000
        @animationTimer = setTimeout((=> @animate()), delay)

    scheduleFetchMessages: ->
        @fetchMessagesTimer = setTimeout((=> @fetchMessages()), 30000)

    scheduleUpdateAllTimes: ->
        setTimeout((=> @updateAllTimes()), 10000)


@Fontana.Visualizer.transitionEffects = transitionEffects
